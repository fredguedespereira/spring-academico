package br.edu.ifpb.pweb2.academico.business.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.ifpb.pweb2.academico.business.model.Aluno;
import br.edu.ifpb.pweb2.academico.dao.AlunoDAO;

@Service
public class AlunoService {
	
	@Autowired
	private AlunoDAO alunoDao;
	
	@Transactional
	public void saveAluno(Aluno aluno) {
		if (aluno.getId() == null) {
			alunoDao.save(aluno);
		} else {
			alunoDao.update(aluno);
		}
	}
	
	public Aluno findAlunoById(Integer id) {
		return alunoDao.findById(id);
	}

	public Object findAll() {
		return alunoDao.findAll();
	}

}
