package br.edu.ifpb.pweb2.academico.spring.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.edu.ifpb.pweb2.academico.business.model.Aluno;
import br.edu.ifpb.pweb2.academico.business.service.AlunoService;

@Controller
@RequestMapping("/alunos")
public class AlunoController {
	
	@Autowired
	private AlunoService businessController;
	
	@RequestMapping("/form")
	public String getForm(Model model) {
		model.addAttribute("aluno", new Aluno());
		return "alunos/form";
	}
	
	@ModelAttribute("paises")
	public List<String> getPaises() {
		return Arrays.asList(new String[] {"Brasil", "Argentina", "Bolívia", "Chile", "Colômbia", "Paraguai", "Peru"});
	}
	
	@RequestMapping("/add")
	public String adicioneAluno(Aluno transacao, Model model) {
		businessController.saveAluno(transacao);
		model.addAttribute("mensagem", "Aluno salvo com sucesso!");
		return this.listeTodos(model);
	}
	
	@RequestMapping("/{id}")
	public String busquePorId(@PathVariable("id") Integer id, Model model) {
		Aluno t = businessController.findAlunoById(id);
		if (t != null) {
			model.addAttribute("aluno", t);
		} else {
			model.addAttribute("mensagem", "Aluno não encontrado!");
			model.addAttribute("aluno", new Aluno());
		}
		return "alunos/form";
	}
	
	@RequestMapping("/list")
	public String listeTodos(Model model) {
		model.addAttribute("alunos", businessController.findAll());
		return "alunos/lista";
	}
	
	

}
