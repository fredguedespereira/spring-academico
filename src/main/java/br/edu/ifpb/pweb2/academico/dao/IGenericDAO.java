package br.edu.ifpb.pweb2.academico.dao;

import java.io.Serializable;
import java.util.List;

public interface IGenericDAO<T extends Serializable, FK> {
	
	T findById(FK id);
	
	List<T> findAll();
	
	void save(final T entity);
	
	T update(final T entity);
	
	void delete(final T entity);
	
	void deleteById(final FK id);
	
	void commit();
	
	void rollback();

}
